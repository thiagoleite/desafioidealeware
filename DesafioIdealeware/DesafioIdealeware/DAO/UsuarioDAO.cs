﻿using DesafioIdealeware.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DesafioIdealeware.DAO
{
    public class UsuarioDAO
    {
        private DesafioContext contexto;

        public UsuarioDAO()
        {
            this.contexto = new DesafioContext();
        }

        public void Salvar(Usuario usuario)
        {
            if (!UsuarioExiste(usuario.Email))
            {
                contexto.Usuarios.Add(usuario);
                contexto.SaveChanges();
            }
        }

        public Usuario Autenticar(string email, string password)
        {
            Usuario usuario = contexto.Usuarios
                .Where(u => u.Email.Equals(email) && u.Password.Equals(password))
                .FirstOrDefault();

            return usuario;
        }

        public bool UsuarioExiste(string email)
        {
            bool existe = contexto.Usuarios
                .Any(u => u.Email.Equals(email));

            return existe;
        }

    }
}