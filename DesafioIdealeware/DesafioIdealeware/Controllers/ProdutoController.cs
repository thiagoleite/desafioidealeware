﻿using DesafioIdealeware.DAO;
using DesafioIdealeware.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace DesafioIdealeware.Controllers
{
    [RoutePrefix("Produtos")]
    public class ProdutoController : ApiController
    {
        private ProdutoDAO dao;

        public ProdutoController()
        {
            dao = new ProdutoDAO();
        }

        [Route]
        [HttpGet]
        [ResponseType(typeof(IEnumerable<Produto>))]
        public HttpResponseMessage GetAll()
        {
            try
            {
                IEnumerable<Produto> produtos = dao.ListarTodos();
                return Request.CreateResponse(HttpStatusCode.OK, produtos);

            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
            

            
        }
    }
}
