﻿using DesafioIdealeware.DAO;
using DesafioIdealeware.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesafioIdealeware.Areas.Admin.Controllers
{
    public class UsuarioController : Controller
    {
        private UsuarioDAO dao;

        public UsuarioController()
        {
            dao = new UsuarioDAO();
        }

        public ActionResult Novo(Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                dao.Salvar(usuario);
                
                return RedirectToAction("Index", "Home", new { usuario = usuario, novo = true });
            }
            else
            {
                return RedirectToAction("Index", "Home", new { usuario = usuario });
            }
        }

        [HttpPost]
        public ActionResult Login(string inputEmail, string inputPassword)
        {
            Usuario usuario = dao.Autenticar(inputEmail, inputPassword);
            if (usuario != null)
            {
                Session["logado"] = usuario;
                return RedirectToAction("Index", "Produto");
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public ActionResult Logout()
        {
            Session.Remove("logado");

            return RedirectToAction("Index", "Home");
        }
    }
}