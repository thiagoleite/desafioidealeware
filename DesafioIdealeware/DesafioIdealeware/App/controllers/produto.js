﻿angular.module('Desafio').controller('produtoController', ['$scope', '$http', '$location', function ($scope, $http, $location) {

    this.ListaProdutos = function () {
        $http({
            method: 'GET',
            url: '/Produtos'
        })
        .then(function (response) {
            $scope.produtos = response.data;
        })
        .catch(function (response) {
            // TODO: Erro quando não conseguir listar os produtos
        });
    };

    this.init = function () {
        $scope.produtos = [];

        this.ListaProdutos();
    };


    this.init();


}]);