﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesafioIdealeware.Models
{
    public class Produto
    {
        public int Id { get; set; }
        [Required]
        public string Nome { get; set; }
        //[Range(0, decimal., ErrorMessage = "O preço deve ser maior do que zero")]
        [DisplayFormat(DataFormatString = "{0:#,##0.000#}", ApplyFormatInEditMode = true)]
        public decimal Preco { get; set; }
        [Range(1, int.MaxValue, ErrorMessage = "A quantidade em estoque não pode ser igual ou menor a zero")]
        public int Estoque { get; set; }
        public string Descricao { get; set; }
    }
}
