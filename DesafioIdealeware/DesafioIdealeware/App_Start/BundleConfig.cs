﻿using System.Web;
using System.Web.Optimization;

namespace DesafioIdealeware
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Admin
            StyleBundle estilos = new StyleBundle("~/bundles/estilos");
            estilos.Include("~/Content/bootstrap.css");
            estilos.Include("~/Content/admin.css");

            bundles.Add(estilos);

            ScriptBundle scripts = new ScriptBundle("~/bundles/scripts");
            scripts.Include("~/Scripts/jquery-1.9.1.js");
            scripts.Include("~/Scripts/bootstrap.js");
            scripts.Include("~/Scripts/jquery.validate.js");
            scripts.Include("~/Scripts/jquery.validate.unobtrusive.js");
            scripts.Include("~/Scripts/JQueryFixes.js");

            bundles.Add(scripts);
            #endregion

            #region Publica
            estilos = new StyleBundle("~/bundles/estilospublica");
            estilos.Include("~/Content/bootstrap.css");
            estilos.Include("~/App/app.css");

            bundles.Add(estilos);

            scripts = new ScriptBundle("~/bundles/scriptspublica");
            scripts.Include("~/Scripts/angular.js");
            scripts.Include("~/Scripts/jquery-1.9.1.js");
            scripts.Include("~/Scripts/bootstrap.js");
            scripts.Include("~/App/app.js");
            scripts.Include("~/App/routes.js");
            scripts.Include("~/App/controllers/produto.js");

            bundles.Add(scripts);
            #endregion


        }
    }
}