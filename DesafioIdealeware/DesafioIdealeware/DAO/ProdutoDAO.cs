﻿using DesafioIdealeware.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DesafioIdealeware.DAO
{
    public class ProdutoDAO
    {
        private DesafioContext context;

        public ProdutoDAO()
        {
            context = new DesafioContext();
        }
        
        public Produto Salvar(Produto produto)
        {
            context.Produtos.Add(produto);
            context.SaveChanges();

            return produto;
        }

        public void Atualizar(Produto produto)
        {
            context.Produtos.Update(produto);
            context.SaveChanges();
        }

        public IEnumerable<Produto> ListarTodos()
        {
            IList<Produto> produtos = new List<Produto>();
            try
            {
                produtos = context.Produtos.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return produtos;
        }

        public Produto ListarPorId(int id)
        {
            Produto produto = context.Produtos.Where(p => p.Id.Equals(id)).FirstOrDefault();
            return produto;
        }

        public IEnumerable<Produto> ListarPorNome(string nome)
        {
            return context.Produtos.Where(p => p.Nome.Contains(nome)).ToList();
        }

        public void Excluir(int id)
        {
            Produto produto = ListarPorId(id);
            context.Produtos.Remove(produto);
            context.SaveChanges();
        }

        
    }
}