﻿using DesafioIdealeware.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesafioIdealeware.Areas.Admin.Controllers
{
    public class HomeController : Controller
    {
        // GET: Admin/Home
        public ActionResult Index(Usuario usuario = null, bool novo = false)
        {
            ViewBag.Title = "Entrar";
            if (usuario == null)
            {
                return View();
            }
            else
            {
                if (novo)
                {
                    ViewBag.Sucesso = "Usuario cadastrado. Utilize seu e-mail e senha para entrar";
                }
                
                return View(usuario);
            }
            
        }


    }
}