﻿using DesafioIdealeware.Areas.Admin.Filters;
using DesafioIdealeware.DAO;
using DesafioIdealeware.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DesafioIdealeware.Areas.Admin.Controllers
{
    [AutorizacaoFilter]
    public class ProdutoController : Controller
    {
        private ProdutoDAO dao;

        public ProdutoController()
        {
            dao = new ProdutoDAO();
        }

        // GET: Admin/Produto
        public ActionResult Index()
        {
            var produtos = dao.ListarTodos();

            ViewBag.Title = "Listar Produtos";

            return View(produtos);
        }
        
        public ActionResult Visualizar(int id, bool alterado = false, bool novo = false)
        {
            Produto produto = dao.ListarPorId(id);
            if (alterado)
            {
                ViewBag.Sucesso = "Produto #" + produto.Id + " alterado com sucesso";
            }

            if (novo)
            {
                ViewBag.Sucesso = "Produto #" + produto.Id + " cadastrado com sucesso";
            }
            
            ViewBag.Title = "Produto: " + produto.Nome;

            return View(produto);
        }
        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Editar(Produto produto)
        {
            dao.Atualizar(produto);
            return RedirectToAction("Visualizar", "Produto", new { id = produto.Id, alterado = true });
        }

        public ActionResult Cadastro()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Novo(Produto produto)
        {
            produto = dao.Salvar(produto);
            return RedirectToAction("Visualizar", "Produto", new { id = produto.Id, novo = true });
        }

        [HttpGet]
        public ActionResult Pesquisa(string nome)
        {
            ViewBag.nome = nome;
            ViewBag.Title = "Pesquisar produtos com nome: " + nome;
            var produtos = dao.ListarPorNome(nome);
            return View(produtos);
        }

        [HttpPost]
        public ActionResult Excluir(int id)
        {
            try
            {
                dao.Excluir(id);
                return Json(true);
            }
            catch (Exception)
            {
                return Json(false);
            }
        }
    }
}